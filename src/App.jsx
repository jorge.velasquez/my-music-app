
import { Provider } from 'react-redux';
import { LoginRouter } from './routers/LoginRouter.jsx';

import store from './store/store.js';

export const App = () => {

    


    return (
        <Provider store={store}>
            <LoginRouter />
        </Provider>
    );
};
