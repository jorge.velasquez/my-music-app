import React from 'react'

export const ButtonLogin = ({handleOnClickLogin}) => {
  return (
      <button className="logout self-center" onClick={handleOnClickLogin}>
          Login
      </button>
  );
}
