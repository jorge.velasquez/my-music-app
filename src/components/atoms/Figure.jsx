
export const Figure = ({classNameFigure, classNameImg, src, alt}) => {
  return (
      <figure className={classNameFigure}>
          <img
              className={classNameImg}
              src={src}
              alt={alt}
          />
      </figure>
  );
}
