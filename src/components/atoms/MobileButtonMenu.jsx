import React from 'react';

export const MobileButtonMenu = ({ link }) => {
    return (
        <button className={link.className} onClick={link.handleMenu}>
            <span
                className={link.menu ? 'line translate-y-3 rotate-45' : 'line'}
            ></span>
            <span className={link.menu ? 'hidden' : 'line'}></span>
            <span
                className={link.menu ? 'line -translate-x -rotate-45' : 'line'}
            ></span>
        </button>
    );
};
