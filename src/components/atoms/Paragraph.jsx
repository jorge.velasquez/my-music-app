import React from 'react'

export const Paragraph = ({title, name}) => {
  return (
      <p className='paragraph'>
          <b>{title} </b> {name}
      </p>
  );
}
