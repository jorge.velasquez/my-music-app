import favoriteUnselected from '../../assets/favoritoNoSave.png';
import favoriteSelected from '../../assets/favoritoSave.png';
import { useButtonFavorites } from '../../hooks/useButtonFavorite';
import { Figure } from '../atoms/Figure';

export const ButtonFavorite = ({ track }) => {
    const [
        handleOnClickFavorite,
    ] = useButtonFavorites();

    return (
        <button
            className="favorite"
            onClick={() => handleOnClickFavorite(track)}
        >
            <Figure
                classNameFigure={'w-11 h-11 self-center flex'}
                classNameImg={'w-full self-center'}
                src={track.favorite ? favoriteSelected : favoriteUnselected}
                alt={'favorites'}
            />
        </button>
    );
};
