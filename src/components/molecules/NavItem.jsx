import React from 'react';
import { Link } from 'react-router-dom';
import { Figure } from '../atoms/Figure';

export const NavItem = ({
    href,
    className,
    text,
    img = undefined,
    logout = undefined,
}) => {
    return (
        <li>
            <Link to={href} className={className} onClick={logout && logout}>
                {text}
                {img && (
                    <Figure
                        classNameFigure="ml-2 w-14 h-14"
                        classNameImg="h-full"
                        src={img.url}
                        alt={img.text}
                    />
                )}
            </Link>
        </li>
    );
};
