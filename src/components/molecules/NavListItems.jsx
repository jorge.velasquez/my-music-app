import React from 'react';
import { MobileButtonMenu } from '../atoms/MobileButtonMenu';
import { NavItem } from './NavItem';

export const NavListItems = ({divClassName, ulClassName, items, link=null}) => {
    return (
        <section className={divClassName}>
            {link !== null ? <MobileButtonMenu link={link}/> : null}
            <ul className={ulClassName}>
                {items.map((item, index) => (
                    <NavItem
                        key={index}
                        href={item.href}
                        className={item.className}
                        text={item.text}
                        img={item?.img}
                        logout={item?.logout}
                    />
                ))}
            </ul>
        </section>
    );
};
