import React from 'react';
import { Link } from 'react-router-dom';
import { Figure } from '../atoms/Figure';

export const NavLogo = ({ logo, href }) => {
    return (
        <Link to={href} className="nav__logo">
            <Figure
                classNameFigure='logo'
                classNameImg='logo__img'
                src={logo}
                alt='MyMusicApp'
            />
        </Link>
    );
};
