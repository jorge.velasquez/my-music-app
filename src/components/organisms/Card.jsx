import { ButtonFavorite } from '../molecules/ButtonFavorite';
import { Paragraph } from '../atoms/Paragraph';
import { Figure } from '../atoms/Figure';

export const Card = ({ track, index, selectedFavorite }) => {
    return (
        <section
            key={track.name + index}
            className="card"
        >
            <Figure classNameFigure={'card__figure'} classNameImg={"w-full" } src={track.album.images[0].url} alt={track.name} />
            <Paragraph title="Titulo: " name={track.name} />
            <Paragraph title="Artista: " name={track.artists[0].name} />
            <ButtonFavorite
                track={track}
                index={index}
                selectedFavorite={selectedFavorite}
            />
        </section>
    );
};
