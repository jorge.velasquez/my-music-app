import { NavLogo } from '../molecules/NavLogo';
import logo from '../../assets/mymusicapp2.png';
import { NavListItems } from '../molecules/NavListItems';

import { useNavBar } from '../../hooks/useNavBar';

export const NavBar = () => {
    const [menu, link, itemsLeft, itemsRight, itemsMobileMenu] = useNavBar();

    return (
        <nav className="nav">
            <NavLogo logo={logo} href="/home" />
            <NavListItems
                divClassName={'links'}
                ulClassName={'menu'}
                items={itemsLeft}
            />
            <NavListItems
                divClassName={'links links--txright'}
                ulClassName={'menu'}
                items={itemsRight}
            />
            <NavListItems
                divClassName={'mobile'}
                ulClassName={
                    menu ? 'mobile__links' : 'mobile__links mobile__links--hidden'
                }
                items={itemsMobileMenu}
                link={link}
            />
        </nav>
    );
};
