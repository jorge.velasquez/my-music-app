import { useSelector } from 'react-redux';
import { Card } from '../organisms/Card';
import { LayoutPages } from '../templates/LayoutPages';

export const Favorites = () => {
    
    const {
        spotifyFavorites,
    } = useSelector((state) => state.users);

    return (
        <LayoutPages>
            <main className="main">
                {spotifyFavorites?.map((track, index) => (
                    <Card
                        key={track.name + index}
                        track={track}
                        index={index}
                        selectedFavorite={true}
                    />
                ))}
            </main>
        </LayoutPages>
    );
};
