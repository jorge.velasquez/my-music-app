import { Card } from '../organisms/Card';
import { useHome } from '../../hooks/useHome';
import { LayoutPages } from '../templates/LayoutPages';

export const Home = () => {
    const [spotifyTracks] = useHome();
    return (
        <LayoutPages>
            <main className="main">
                {spotifyTracks?.map((track, index) => (
                    <Card
                        key={track.name + index}
                        track={track}
                        index={index}
                    />
                ))}
            </main>
        </LayoutPages>
    );
};
