import { useLogin } from '../../hooks/useLogin';
import { ButtonLogin } from '../atoms/ButtonLogin';
import { Figure } from '../atoms/Figure';

import logoSpotify from '../../assets/Spotify-symbol.jpg'

export const Login = () => {
    const [ handleOnClickLogin ] = useLogin()
    return (
        <main className="login">
            <Figure
                classNameFigure=""
                classNameImg=""
                src={logoSpotify}
                alt="Logo Spotify"
            />
            <ButtonLogin handleOnClickLogin={handleOnClickLogin} />
        </main>
    );
};
