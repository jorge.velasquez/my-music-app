import { NavBar } from '../organisms/NavBar';

export const LayoutPages = ({ children }) => {
    return (
        <>
            <NavBar />
            {children}
        </>
    );
};
