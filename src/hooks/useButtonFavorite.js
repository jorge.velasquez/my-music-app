import { useDispatch, useSelector } from 'react-redux';
import {
    fetchApiSaveTracks,
    setSpotifyFavorites,
    setSpotifyTracks,
} from '../store/slices/users';

export const useButtonFavorites = () => {
    const dispatch = useDispatch();
    const { spotifyTracks, spotifyFavorites, spotifyTokenResponse } =
        useSelector((state) => state.users);

    const saveStorageRedux = (favorites, tracks = undefined) => {
        dispatch(setSpotifyFavorites(favorites));
        localStorage.setItem(
            'spotifyFavorites',
            JSON.stringify({ value: favorites })
        );
        if (tracks) {
            dispatch(setSpotifyTracks(tracks));
            localStorage.setItem(
                'spotifyTracks',
                JSON.stringify({ value: tracks })
            );
        }
    };

    const handleOnClickFavorite = (element) => {
        const isFavorite = spotifyFavorites.find(
            (track) => track.id === element.id && true
        );

        const deleteTrack = async () => {
            await dispatch(
                fetchApiSaveTracks({
                    token: spotifyTokenResponse,
                    method: 'DELETE',
                    paramsArray: [{ ids: element.id }],
                })
            );
            const favorites = spotifyFavorites.filter(
                (track) => track.id !== element.id
            );

            const tracks = spotifyTracks.map((element) =>
                favorites.find((track) =>
                    track.id === element.id ? true : false
                )
                    ? { ...element, favorite: true }
                    : { ...element, favorite: false }
            );
            saveStorageRedux(favorites, tracks);
        };

        const addTrack = async () => {
            const tracks = spotifyTracks.map((track) =>
                track.id === element.id ? { ...element, favorite: true } : track
            );
            dispatch(setSpotifyTracks(tracks));

            localStorage.setItem(
                'spotifyTracks',
                JSON.stringify({ value: tracks })
            );

            await dispatch(
                fetchApiSaveTracks({
                    token: spotifyTokenResponse,
                    method: 'PUT',
                    paramsArray: [{ ids: element.id }],
                })
            );

            const { payload } = await dispatch(
                fetchApiSaveTracks({
                    token: spotifyTokenResponse,
                    method: 'GET',
                    paramsArray: [],
                })
            );

            const favorites = payload?.items.map(({ track }) => ({
                ...track,
                favorite: true,
            }));
            
            saveStorageRedux(favorites);
        };

        if (isFavorite) {
            deleteTrack();
        } else {
            addTrack();
        }
    };

    return [handleOnClickFavorite];
};
