import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
    fetchApiSaveTracks,
    fetchApiSearch,
    setIsAuthenticated,
    setSpotifyDataUser,
    setSpotifyFavorites,
    setSpotifyRefreshToken,
    setSpotifyTokenResponse,
    setSpotifyTracks,
} from '../store/slices/users';

export const useHome = () => {
    const { spotifyTokenResponse, spotifyTracks, spotifyFavorites } =
        useSelector((state) => state.users);
    const dispatch = useDispatch();

    const paramsArray = [
        {
            q: ' ' + String.fromCharCode(Math.ceil(Math.random() * 25) + 97),
        },
        {
            type: 'track',
        },
        {
            offset: 50,
        },
    ];

    const tracksCall = async () => {
        const { payload } = await dispatch(
            fetchApiSearch({ paramsArray, spotifyTokenResponse })
        );
        if (payload?.error) {
            localStorage.clear();
            dispatch(setIsAuthenticated(false));
            dispatch(setSpotifyRefreshToken(undefined));
            dispatch(setSpotifyTokenResponse(undefined));
            dispatch(setSpotifyDataUser(undefined));
        } else {
            const { tracks } = payload;

            if (spotifyTracks.length === 0) {
                dispatch(
                    setSpotifyTracks(
                        tracks?.items.map((track) => ({
                            ...track,
                            favorite: false,
                        }))
                    )
                );
                localStorage.setItem(
                    'spotifyTracks',
                    JSON.stringify({
                        value: tracks?.items.map((track) => ({
                            ...track,
                            favorite: false,
                        })),
                    })
                );
            }
        }
    };

    const saveTracks = async () => {
        const { payload } = await dispatch(
            fetchApiSaveTracks({
                token: spotifyTokenResponse,
                method: 'GET',
                paramsArray: [],
            })
        );
        dispatch(
            setSpotifyFavorites(
                payload?.items?.map(({ track }) => ({
                    ...track,
                    favorite: true,
                }))
            )
        );
        localStorage.setItem(
            'spotifyFavorites',
            JSON.stringify({ value: spotifyFavorites })
        );
    };

    useEffect(() => {
        saveTracks();
        !JSON.parse(localStorage.getItem('spotifyTracks'))?.value &&
            tracksCall();

        JSON.parse(localStorage.getItem('spotifyTracks'))?.value.length === 0 &&
            tracksCall();

        dispatch(
            setSpotifyTracks(
                JSON.parse(localStorage.getItem('spotifyTracks'))?.value
            )
        );
    }, []);

    return [spotifyTracks]
}