import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useLocation } from 'react-router-dom';
import {
    fetchApi,
    fetchApiUserData,
    setIsAuthenticated,
    setSpotifyRefreshToken,
    setSpotifyTokenResponse,
    setSpotifyDataUser,
} from '../store/slices/users';

export const useLogin = () => {
    const location = useLocation();

    const { isAuthenticated } = useSelector((state) => state.users);

    const dispatch = useDispatch();
    const handleOnClickLogin = () => {
        window.location.replace(loginURL);
    };

    const intro = { intro: false };
    const endPoint = 'https://accounts.spotify.com/authorize';
    const scope = [
        'user-read-recently-played',
        'user-read-playback-state',
        'user-top-read',
        'user-modify-playback-state',
        'user-library-modify',
        'user-library-read',
    ];
    const loginURL = `${endPoint}?client_id=${
        process.env.REACT_APP_SPOTIFY_CLIENT_ID
    }&response_type=code&redirect_uri=${
        process.env.REACT_APP_SPOTIFY_CALLBACK_HOST
    }&scope=${scope.join('%20')}`;

    const callApi = async (params) => {
        const { payload: response } = await dispatch(fetchApi(params));
        if (response?.access_token) {
            if (response.refresh_token)
                dispatch(setSpotifyRefreshToken(response.refresh_token));
            dispatch(setSpotifyTokenResponse(response.access_token));
            dispatch(setIsAuthenticated(true));
            localStorage.setItem(
                'isAuthenticated',
                JSON.stringify({ value: true })
            );
            localStorage.setItem(
                'spotifyRefreshToken',
                JSON.stringify({ value: response?.refresh_token })
            );
            localStorage.setItem(
                'spotifyTokenResponse',
                JSON.stringify({ value: response.access_token })
            );
            const { payload: dataUser } = await dispatch(
                fetchApiUserData(response.access_token)
            );
            const { display_name, id, images } = dataUser;
            const { url } = images[0];
            dispatch(setSpotifyDataUser({ display_name, id, url }));
            localStorage.setItem(
                'spotifyDataUser',
                JSON.stringify({ value: { display_name, id, url } })
            );
        }
    };

    useEffect(() => {
        dispatch(
            setIsAuthenticated(
                JSON.parse(localStorage.getItem('isAuthenticated'))?.value ||
                    false
            )
        );
        dispatch(
            setSpotifyRefreshToken(
                JSON.parse(localStorage.getItem('spotifyRefreshToken'))
                    ?.value || undefined
            )
        );
        dispatch(
            setSpotifyTokenResponse(
                JSON.parse(localStorage.getItem('spotifyTokenResponse'))
                    ?.value || undefined
            )
        );
        dispatch(
            setSpotifyDataUser(
                JSON.parse(localStorage.getItem('spotifyDataUser'))?.value ||
                    undefined
            )
        );
        const urlParams = new URLSearchParams(location.search);
        const spotifyCode = urlParams.get('code');
        if ((spotifyCode || isAuthenticated) && intro.intro === false) {
            callApi({ spotifyCode });
            intro.intro = true;
        }
    }, [location.search]);

    return [handleOnClickLogin]
};
