import { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
    setIsAuthenticated,
    setSpotifyDataUser,
    setSpotifyRefreshToken,
    setSpotifyTokenResponse,
    setSpotifyTracks,
} from '../store/slices/users';

export const useNavBar = () => {
    const dispatch = useDispatch();

    const [menu, setMenu] = useState(false);

    const handleMenu = () => {
        setMenu(!menu);
    };

    const handleOnClickLogout = () => {
        dispatch(setIsAuthenticated(false));
        dispatch(setSpotifyDataUser(undefined));
        dispatch(setSpotifyTokenResponse(undefined));
        dispatch(setSpotifyRefreshToken(undefined));
        dispatch(setSpotifyTracks([]))
        localStorage.clear();
    };

    const { spotifyDataUser } = useSelector((state) => state.users);

    const itemsLeft = [
        { className: 'link', href: '/home', text: 'Inicio' },
        { className: 'link', href: '/favorites', text: 'Favoritos' },
    ];

    const link = {
        href: '#',
        className: 'link',
        text: 'Menu',
        menu,
        handleMenu,
    };

    const itemsRight = [
        {
            className: 'link-image',
            href: '#',
            text: spotifyDataUser?.display_name,
            img: { url: spotifyDataUser?.url, text: 'Profile' },
        },
        {
            className: 'logout',
            href: '/',
            text: 'Salir',
            logout: handleOnClickLogout,
        },
    ];

    const itemsMobileMenu = [
        { className: 'link', href: '/home', text: 'Inicio' },
        { className: 'link', href: '/favorites', text: 'Favoritos' },
        {
            className: 'link-image',
            href: '#',
            text: spotifyDataUser?.display_name,
            img: { url: spotifyDataUser?.url, text: 'Profile' },
        },
        {
            className: 'logout my-4 inline-block align-middle',
            href: '/',
            text: 'Salir',
            logout: handleOnClickLogout,
        },
    ];

    return [
        menu,
        link,
        itemsLeft,
        itemsRight,
        itemsMobileMenu,
    ];
};
