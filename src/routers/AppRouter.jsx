import React from 'react';
import { Route, Routes } from 'react-router-dom';
import { Favorites } from '../components/pages/Favorites';
import { Home } from '../components/pages/Home';

export const AppRouter = () => {
    return (
        <Routes>
            <Route end path="/home" element={<Home />} />
            <Route end path="/favorites" element={<Favorites />} />
        </Routes>
    );
};
