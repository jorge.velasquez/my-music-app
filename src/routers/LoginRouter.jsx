import { BrowserRouter as Router, Route, Routes } from 'react-router-dom'
import { Login } from '../components/pages/Login'
import { AppRouter } from './AppRouter'
import { PrivateRouter } from './PrivateRouter'
import { PublicRouter } from './PublicRouter'

export const LoginRouter = () => {
    return (
        <Router>
            <Routes>
                <Route
                    end
                    path="/"
                    element={
                        <PublicRouter>
                            <Login />
                        </PublicRouter>
                    }
                />
                {/* <Route end path="/favorites" element={<Favorites />} /> */}
                <Route
                    path="*"
                    element={
                        <PrivateRouter>
                            <AppRouter />
                        </PrivateRouter>
                    }
                />
            </Routes>
        </Router>
    );
}
