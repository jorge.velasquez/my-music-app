import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import {
    spotifyAuthCall,
    spotifySaveTracks,
    spotifySearchCall,
    spotifyUserData,
} from '../../../helpers/apiCalls';

export const userSlices = createSlice({
    name: 'users',
    initialState: {
        isAuthenticated: false,
        spotifyRefreshToken: undefined,
        spotifyTokenResponse: undefined,
        spotifyDataUser: undefined,
        spotifyTracks: [],
        spotifyFavorites: [],
    },
    reducers: {
        setIsAuthenticated: (state, action) => {
            state.isAuthenticated = action.payload;
        },
        setSpotifyRefreshToken: (state, action) => {
            state.spotifyRefreshToken = action.payload;
        },
        setSpotifyTokenResponse: (state, action) => {
            state.spotifyTokenResponse = action.payload;
        },
        setSpotifyDataUser: (state, action) => {
            state.spotifyDataUser = action.payload;
        },
        setSpotifyTracks: (state, action) => {
            state.spotifyTracks = action.payload;
        },
        setSpotifyFavorites: (state, action) => {
            state.spotifyFavorites = action.payload;
        },
    },
});

export const {
    setIsAuthenticated,
    setSpotifyRefreshToken,
    setSpotifyTokenResponse,
    setSpotifyDataUser,
    setSpotifyTracks,
    setSpotifyFavorites,
} = userSlices.actions;

export default userSlices.reducer;

export const fetchApi = createAsyncThunk(
    'users/spotifyAuthCall',
    async (params) => {
        try {
            let response;

            if (params?.spotifyRefreshToken) {
                response = await spotifyAuthCall({
                    refresh_token: params?.spotifyRefreshToken,
                    grant_type: 'refresh_token',
                });
            } else
                response = await spotifyAuthCall({
                    code: params?.spotifyCode,
                    grant_type: 'authorization_code',
                });

            if (response.access_token) {
                return response;
            } else {
                throw new Error('Usuario no fue logeado');
            }
        } catch (error) {
            alert('Usuario no fue logeado');
        }
    }
);

export const fetchApiSearch = createAsyncThunk(
    'users/spotifySearchCall',
    async (params) => {
        const response = await spotifySearchCall(
            params.paramsArray,
            params.spotifyTokenResponse
        );
        return response;
    }
);

export const fetchApiUserData = createAsyncThunk(
    'users/spotifyUserData',
    async (token) => {
        const response = await spotifyUserData(token);
        return response;
    }
);

export const fetchApiSaveTracks = createAsyncThunk(
    'users/spotifySaveTracks',
    async ({ paramsArray, token, method }) => {
        const response = await spotifySaveTracks(paramsArray, token, method);
        return response;
    }
);
