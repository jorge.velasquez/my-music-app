import { configureStore } from "@reduxjs/toolkit";
// Reducers
import users from './slices/users/index'

export default configureStore({
    reducer: {
        users,
    }
})